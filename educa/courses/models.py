from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe

from courses.fields import OrderField


class Subject(models.Model):
    """Модель предмета"""
    title = models.CharField(max_length=200, verbose_name='Название предмета')
    slug = models.SlugField(max_length=200, unique=True)

    class Meta:
        ordering = ['title']
        verbose_name = 'Предмет'
        verbose_name_plural = 'Предметы'

    def __str__(self):
        return self.title


class Course(models.Model):
    """Модель курса"""
    owner = models.ForeignKey(User,
                              on_delete=models.CASCADE,
                              related_name='courses_created',
                              verbose_name='Автор курса')
    subject = models.ForeignKey(Subject,
                                on_delete=models.CASCADE,
                                related_name='courses',
                                verbose_name='Предмет')
    title = models.CharField(max_length=200, verbose_name='Название курса')
    slug = models.SlugField(max_length=200)
    overview = models.TextField(verbose_name='Описание курса')
    created = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания курса')
    students = models.ManyToManyField(User, blank=True, related_name='courses_joined',
                                      verbose_name='Студенты курса')

    class Meta:
        ordering = ['-created']
        verbose_name = 'Курс'
        verbose_name_plural = 'Курсы'

    def __str__(self):
        return self.title


class Module(models.Model):
    """Модель модуля курса"""
    course = models.ForeignKey(Course, on_delete=models.CASCADE,
                               related_name='modules',
                               verbose_name='Курс')
    title = models.CharField(max_length=200, verbose_name='Название модуля')
    description = models.TextField(verbose_name='Описание модуля')
    order = OrderField(blank=True, for_fields=['course'])

    class Meta:
        ordering = ['order']
        verbose_name = 'Модуль'
        verbose_name_plural = 'Модули'

    def __str__(self):
        return '{}. {}'.format(self.order, self.title)


class Content(models.Model):
    """Модель обобщенных типов"""
    module = models.ForeignKey(Module, on_delete=models.CASCADE,
                               related_name='contents', verbose_name='Модель')
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE,
                                     limit_choices_to={'model__in': (
                                         'text',
                                         'video',
                                         'image',
                                         'file')},
                                     verbose_name='Контент')
    object_id = models.PositiveIntegerField()
    item = GenericForeignKey('content_type', 'object_id')
    order = OrderField(blank=True, for_fields=['module'])

    class Meta:
        ordering = ['order']


class ItemBase(models.Model):
    """Абстрактная модель для моделей контента"""
    owner = models.ForeignKey(User, on_delete=models.CASCADE,
                              related_name='%(class)s_related')
    title = models.CharField(max_length=250, verbose_name='Название')
    created = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    updated = models.DateTimeField(auto_now=True, verbose_name='Дата изменения')

    def render(self):
        return render_to_string('courses/content/{}.html'.format(self._meta.model_name), {'item': self})

    class Meta:
        abstract = True

    def __str__(self):
        return self.title


class Text(ItemBase):
    """Модель для контента типа текст"""
    content = models.TextField(verbose_name='Текст')


class File(ItemBase):
    """Модель для контента типа файл"""
    file = models.FileField(upload_to='files', verbose_name='Файл')


class Image(ItemBase):
    """Модель для контента типа картинка"""
    file = models.FileField(upload_to='images', verbose_name='Картинка')


class Video(ItemBase):
    """Модель для контента типа видео"""
    url = models.URLField(verbose_name='Видео')
