from django import forms

from courses.models import Course


class CourseEnrollForm(forms.Form):
    """Форма для записи студентов на курс"""
    course = forms.ModelChoiceField(queryset=Course.objects.all(),
                                    widget=forms.HiddenInput)
